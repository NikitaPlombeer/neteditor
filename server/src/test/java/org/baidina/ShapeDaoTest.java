package org.baidina;

import org.baidina.dao.ShapeDao;
import org.baidina.dao.ShapeDaoImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ShapeDaoTest {

    private ShapeDao shapeDao;

    @BeforeEach
    public void beforeTest() {
        shapeDao = new ShapeDaoImpl();
    }

    @Test
    public void findShapes_Test() {
        List<MyShape> shapes = shapeDao.findShapes();
        assertEquals(shapes.size(), 0);
        int n = 5;
        for (int i = 0; i < n; i++) {
            shapeDao.add(generateShape());
        }
        shapes = shapeDao.findShapes();
        assertEquals(shapes.size(), n);
    }


    @Test
    public void add_Test() {
        MyShape shape = generateShape();
        shapeDao.add(shape);
        List<MyShape>  shapes = shapeDao.findShapes();
        assertTrue(shapes.contains(shape));
    }

    public MyShape generateShape() {
        return MyShape.newBuilder()
                .id(UUID.randomUUID().toString())
                .strokeColor(Color.BLACK.getRGB())
                .type(MyShape.Type.BRUSH)
                .fillColor(Color.BLACK.getRGB())
                .alpha(1f)
                .coords(new int[]{1, 2, 3, 4})
                .build();
    }
}
