package org.baidina;

import org.baidina.dao.ShapeDao;
import org.baidina.dao.ShapeDaoImpl;
import org.baidina.handler.ShapeControllerImpl;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ShapeControllerImplTest {

    private ShapeDao shapeDao;
    private ShapeControllerImpl shapeController;

    @BeforeAll
    public void beforeTest() {
        shapeDao = new ShapeDaoImpl();
        shapeController = new ShapeControllerImpl(shapeDao);
    }

    @Test
    public void shapeandler_Test() {
        MyShape shape = MyShape.newBuilder()
                .id(UUID.randomUUID().toString())
                .strokeColor(Color.BLACK.getRGB())
                .type(MyShape.Type.BRUSH)
                .fillColor(Color.BLACK.getRGB())
                .alpha(1f)
                .coords(new int[]{1, 2, 3, 4})
                .build();

        shapeController.shapeHandler(shape, new ByteArrayOutputStream());

        assertTrue(shapeDao.findShapes().contains(shape));
    }

    @Test
    public void stringHandler_Test() throws IOException {
        String input = "GET PICTURE";
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        shapeController.stringHandler(input, out);

        ByteArrayOutputStream out2 = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(out2);
        objectOutputStream.writeObject(shapeDao.findShapes());
        objectOutputStream.flush();

        assertArrayEquals(out.toByteArray(), out2.toByteArray());
    }
}
