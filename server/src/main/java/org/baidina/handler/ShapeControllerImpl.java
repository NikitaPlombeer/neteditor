package org.baidina.handler;

import org.baidina.MyShape;
import org.baidina.dao.ShapeDao;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class ShapeControllerImpl implements ShapeController {

    private final ShapeDao shapeDao;

    public ShapeControllerImpl(ShapeDao shapeDao) {
        this.shapeDao = shapeDao;
    }

    @Override
    public void stringHandler(String input, OutputStream socketOut) throws IOException {
        System.out.println("handle string: " + input);
        if(input.equals("GET PICTURE")) {
            ObjectOutputStream out = new ObjectOutputStream(socketOut);
            out.writeObject(shapeDao.findShapes());
            out.flush();
        }
    }

    @Override
    public void shapeHandler(MyShape shape, OutputStream socketOut) {
        System.out.println("Handle shape with id = " + shape.getId());
        shapeDao.add(shape);
    }
}
