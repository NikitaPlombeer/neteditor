package org.baidina.handler;

import org.baidina.MyShape;

import java.io.IOException;
import java.io.OutputStream;

public interface ShapeController {
    void stringHandler(String input, OutputStream socketOut) throws IOException;
    void shapeHandler(MyShape shape, OutputStream socketOut);
}
