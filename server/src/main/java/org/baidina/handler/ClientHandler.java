package org.baidina.handler;

import org.baidina.MyShape;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class ClientHandler implements Runnable {
    private final Socket clientSocket;
    private final ShapeController shapeController;

    ClientHandler(Socket clientSocket, ShapeController shapeController) {
        this.clientSocket = clientSocket;
        this.shapeController = shapeController;
    }

    @Override
    public void run() {
        System.out.println("Got a client !");

        ObjectInputStream in;
        try {
            in = new ObjectInputStream(this.clientSocket.getInputStream());
            Object input = in.readObject();
            if (input != null) {
                System.out.println("Request from client received!");
                if (input instanceof String) {
                    shapeController.stringHandler((String) input, clientSocket.getOutputStream());
                } else if (input instanceof MyShape) {
                    shapeController.shapeHandler((MyShape) input, clientSocket.getOutputStream());
                }
            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}