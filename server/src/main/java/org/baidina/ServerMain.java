package org.baidina;

import org.baidina.dao.ShapeDao;
import org.baidina.dao.ShapeDaoImpl;
import org.baidina.handler.ShapeController;
import org.baidina.handler.PaintServer;
import org.baidina.handler.ShapeControllerImpl;

public class ServerMain {
    public static void main(String[] args) {
        ShapeDao shapeDao = new ShapeDaoImpl();
        ShapeController shapeController = new ShapeControllerImpl(shapeDao);
        PaintServer paintServer = new PaintServer(shapeController);
        paintServer.startServer();
    }
}
