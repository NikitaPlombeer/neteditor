package org.baidina.dao;

import org.baidina.MyShape;

import java.util.List;

public interface ShapeDao {

    List<MyShape> findShapes();

    void add(MyShape shape);
}
