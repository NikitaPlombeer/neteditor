package org.baidina.dao;

import org.baidina.MyShape;

import java.util.ArrayList;
import java.util.List;

public class ShapeDaoImpl implements ShapeDao {
    private List<MyShape> shapes;


    public ShapeDaoImpl() {
        this.shapes = new ArrayList<>();
    }

    @Override
    public List<MyShape> findShapes() {
        return shapes;
    }

    @Override
    public void add(MyShape shape) {
        shapes.add(shape);
    }
}
