package org.baidina;

import java.io.Serializable;
import java.util.Arrays;
import java.util.UUID;

public class MyShape implements Serializable {
    private String id;
    private Type type;
    private int fillColor;
    private int strokeColor;
    private float alpha;
    private int coords[];

    private MyShape(Builder builder) {
        if(builder.id != null)
            id = builder.id;
        else {
            id = UUID.randomUUID().toString();
        }
        type = builder.type;
        fillColor = builder.fillColor;
        strokeColor = builder.strokeColor;
        alpha = builder.alpha;
        coords = builder.coords;
    }

    public String getId() {
        return id;
    }

    public Type getType() {
        return type;
    }

    public int getFillColor() {
        return fillColor;
    }

    public int getStrokeColor() {
        return strokeColor;
    }

    public float getAlpha() {
        return alpha;
    }

    public int[] getCoords() {
        return coords;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MyShape myShape = (MyShape) o;

        if (fillColor != myShape.fillColor) return false;
        if (strokeColor != myShape.strokeColor) return false;
        if (Float.compare(myShape.alpha, alpha) != 0) return false;
        if (!id.equals(myShape.id)) return false;
        if (type != myShape.type) return false;
        return Arrays.equals(coords, myShape.coords);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + fillColor;
        result = 31 * result + strokeColor;
        result = 31 * result + (alpha != +0.0f ? Float.floatToIntBits(alpha) : 0);
        result = 31 * result + Arrays.hashCode(coords);
        return result;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public enum Type {
        LINE, ELLIPSE, RECTANGLE, BRUSH
    }


    public static final class Builder {
        private String id;
        private Type type;
        private int fillColor;
        private int strokeColor;
        private float alpha;
        private int[] coords;

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder type(Type val) {
            type = val;
            return this;
        }

        public Builder fillColor(int val) {
            fillColor = val;
            return this;
        }

        public Builder strokeColor(int val) {
            strokeColor = val;
            return this;
        }

        public Builder alpha(float val) {
            alpha = val;
            return this;
        }

        public Builder coords(int[] val) {
            coords = val;
            return this;
        }

        public MyShape build() {
            return new MyShape(this);
        }
    }
}
