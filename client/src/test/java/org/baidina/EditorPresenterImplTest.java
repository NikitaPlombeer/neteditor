package org.baidina;

import org.baidina.mvp.EditorPresenterImpl;
import org.baidina.net.SocketClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;

public class EditorPresenterImplTest {
    private EditorPresenterImpl editorPresenter;

    @Mock
    private SocketClient socketClient;

    @BeforeEach
    public void beforeTest() {
        MockitoAnnotations.initMocks(this);
        editorPresenter = new EditorPresenterImpl(socketClient);
    }

    @Test
    public void setters_test() {
        editorPresenter.setCurrentAction(2);
        assertEquals(2, editorPresenter.getCurrentAction());

        editorPresenter.setTransparentValue(0.1f);
        assertEquals(0.1f, editorPresenter.getTransparentValue());

        editorPresenter.setFillColor(Color.BLUE);
        assertEquals(Color.BLUE.getRGB(), editorPresenter.getFillColor().getRGB());

        editorPresenter.setStrokeColor(Color.CYAN);
        assertEquals(Color.CYAN.getRGB(), editorPresenter.getStrokeColor().getRGB());

    }


    @Test
    public void test() {
        editorPresenter.setCurrentAction(1);
        doNothing().when(socketClient).postNewShapeAsync(any(Object.class));
        String id = editorPresenter.createNewShape(1, 2, 3, 4);

        MyShape shape = MyShape.newBuilder()
                .id(id)
                .type(MyShape.Type.BRUSH)
                .strokeColor(editorPresenter.getStrokeColor().getRGB())
                .fillColor(editorPresenter.getFillColor().getRGB())
                .alpha(editorPresenter.getTransparentValue())
                .coords(new int[]{1, 2, 3, 4})
                .build();

        assertTrue(editorPresenter.getShapes().contains(shape));
    }
}
