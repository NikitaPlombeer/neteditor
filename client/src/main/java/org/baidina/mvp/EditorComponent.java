package org.baidina.mvp;

import org.baidina.MyShape;
import org.baidina.utils.ShapeDrawer;
import org.baidina.utils.ShapeFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

public class EditorComponent extends JComponent {
    //Точка начала рисования
    private Point drawStart;

    //Точка конца рисования
    private Point drawEnd;

    private final EditorPresenter editorPresenter;

    public EditorComponent(EditorPresenter editorPresenter) {
        this.editorPresenter = editorPresenter;
        this.addMouseListener(new MouseAdapter() {

            public void mousePressed(MouseEvent e) {
                if (editorPresenter.getCurrentAction() != 1) { //если не кисть
                    drawStart = new Point(e.getX(), e.getY()); // запоминаются координаты мышки
                    drawEnd = drawStart;
                    repaint(); //рисует
                    editorPresenter.mouseStartPainting();//mouseMoved.set(true);
                }
            }

            public void mouseReleased(MouseEvent e) { //кнопка отпущена
                final int currentAction = editorPresenter.getCurrentAction();
                if (currentAction != 1) {
                    editorPresenter.createNewShape(drawStart.x, drawStart.y, e.getX(), e.getY());

                    drawStart = null;
                    drawEnd = null;

                    repaint();
                    editorPresenter.mouseStopPainting();//mouseMoved.set(false);
                }
            }
        });
        this.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) { //мышь двигается, добавляем лисенер на движение мыши

                if (editorPresenter.getCurrentAction() == 1) {
                    int x = e.getX();
                    int y = e.getY();

                    editorPresenter.setStrokeColor(editorPresenter.getFillColor());
                    editorPresenter.createNewShape(x, y, 5, 5);
                    repaint();
                }
                drawEnd = new Point(e.getX(), e.getY());
                repaint();
            }
        });
    }

    @Override
    public void paint(Graphics g) { // переопределяем метод.

        Graphics2D graphSettings = (Graphics2D) g;

        graphSettings.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        graphSettings.setStroke(new BasicStroke(4));

        editorPresenter.shapesStartIterating();
        for (MyShape myShape : editorPresenter.getShapes()) {
            ShapeDrawer.draw(graphSettings, myShape);
        }
        editorPresenter.shapesStopIterating();


        //если еще не отпустили мышь, то будем рисовать только контур
        if (drawStart != null && drawEnd != null) {

            graphSettings.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.40f));

            graphSettings.setPaint(Color.LIGHT_GRAY);
            Shape aShape = null;
            int currentAction = editorPresenter.getCurrentAction();
            if (currentAction == 2) {
                aShape = ShapeFactory.createLine(drawStart.x, drawStart.y,
                        drawEnd.x, drawEnd.y);
            } else if (currentAction == 3) {
                aShape = ShapeFactory.createEllipse(drawStart.x, drawStart.y,
                        drawEnd.x, drawEnd.y);
            } else if (currentAction == 4) {
                aShape = ShapeFactory.createRectangle(drawStart.x, drawStart.y,
                        drawEnd.x, drawEnd.y);
            }
            graphSettings.draw(aShape);

        }
    }
}
