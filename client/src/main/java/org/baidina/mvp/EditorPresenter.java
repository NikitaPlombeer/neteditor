package org.baidina.mvp;

import org.baidina.MyShape;

import java.awt.Color;
import java.util.List;

public interface EditorPresenter {

    void setCurrentAction(int action);
    int getCurrentAction();

    void setTransparentValue(float alpha);
    float getTransparentValue();

    Color getStrokeColor();
    void setStrokeColor(Color color);

    Color getFillColor();
    void setFillColor(Color color);

    void mouseStartPainting();

    void mouseStopPainting();

    String createNewShape(int x1, int y1, int x2, int y2);

    List<MyShape> getShapes();

    void shapesStartIterating();

    void shapesStopIterating();

    void setEditorView(EditorView editorView);

    void startListening();
}
