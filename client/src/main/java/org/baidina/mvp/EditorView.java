package org.baidina.mvp;

public interface EditorView {

    void repaintShapes();

    void showView();
}
