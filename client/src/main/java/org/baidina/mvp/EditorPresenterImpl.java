package org.baidina.mvp;

import org.baidina.MyShape;
import org.baidina.net.SocketClient;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class EditorPresenterImpl implements EditorPresenter {
    private static final AtomicBoolean mouseMoved = new AtomicBoolean(false);
    private static final AtomicBoolean iteratingShapes = new AtomicBoolean(false);
    private static final Object SYNC_LIST_SEMAPHORE = new Object();

    private int currentAction = 1;
    private float alpha = 1.0f;
    private Color strokeColor = Color.BLACK;
    private Color fillColor = Color.BLACK;
    private List<MyShape> shapes;
    private EditorView editorView;
    private SocketClient socketClient;

    public EditorPresenterImpl(SocketClient socketClient) {
        this.socketClient = socketClient;
        shapes = new ArrayList<>();
    }

    @Override
    public void setCurrentAction(int action) {
        this.currentAction = action;
    }

    @Override
    public int getCurrentAction() {
        return currentAction;
    }

    @Override
    public void setTransparentValue(float alpha) {
        this.alpha = alpha;
    }

    @Override
    public float getTransparentValue() {
        return alpha;
    }

    @Override
    public Color getStrokeColor() {
        return strokeColor;
    }

    @Override
    public void setStrokeColor(Color strokeColor) {
        this.strokeColor = strokeColor;
    }

    @Override
    public Color getFillColor() {
        return fillColor;
    }

    @Override
    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;

    }

    @Override
    public void mouseStartPainting() {
        System.out.println("mouseStartPainting");
        mouseMoved.set(true);
    }

    @Override
    public void mouseStopPainting() {
        System.out.println("mouseStopPainting");
        mouseMoved.set(false);
    }

    @Override
    public String createNewShape(int x1, int y1, int x2, int y2) {
        MyShape.Builder sBuilder = MyShape.newBuilder();

        if (currentAction == 1) {
            sBuilder.type(MyShape.Type.BRUSH);
        } else if (currentAction == 2) {
            sBuilder.type(MyShape.Type.LINE);
        } else if (currentAction == 3) {
            sBuilder.type(MyShape.Type.ELLIPSE);
        } else if (currentAction == 4) {
            sBuilder.type(MyShape.Type.RECTANGLE);
        }
        MyShape shape = sBuilder.strokeColor(getStrokeColor().getRGB())
                .id(UUID.randomUUID().toString())
                .fillColor(getFillColor().getRGB())
                .alpha(getTransparentValue())
                .coords(new int[]{x1, y1, x2, y2})
                .build();


        shapes.add(shape);
        socketClient.postNewShapeAsync(shape);
        return shape.getId();
    }

    @Override
    public List<MyShape> getShapes() {
        return shapes;
    }

    @Override
    public void shapesStartIterating() {
        iteratingShapes.set(true);
    }

    @Override
    public void shapesStopIterating() {
        iteratingShapes.set(false);
    }

    @Override
    public void setEditorView(EditorView editorView) {
        this.editorView = editorView;
    }

    @Override
    public void startListening() {
        new PictureUpdater(shapes -> {
            this.shapes = shapes.stream().filter(Objects::nonNull).collect(Collectors.toList());
            editorView.repaintShapes();
        }).start();
    }


    private interface OnShapeReceivedCallback {
        void onReceive(List<MyShape> shapes);
    }

    private class PictureUpdater extends Thread {


        private final OnShapeReceivedCallback callback;

        public PictureUpdater(OnShapeReceivedCallback callback) {
            this.callback = callback;
        }

        public void run() {
            while (true) {
                if(!mouseMoved.get() && !iteratingShapes.get()) {
                    Future<List<MyShape>> async = socketClient.getPictureAsync();
                    synchronized (SYNC_LIST_SEMAPHORE) {
                        try {
                            if(async.get() != null) {
                                callback.onReceive(async.get());
                            }
                        } catch (InterruptedException | ExecutionException e) {
                            e.printStackTrace();
                            return;
                        }
                    }
                } else {
                    System.out.println("Mouse is still moving OR shapes are being painted");
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
