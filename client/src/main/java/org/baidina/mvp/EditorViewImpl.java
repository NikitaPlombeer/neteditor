package org.baidina.mvp;

import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;

public class EditorViewImpl extends JFrame implements EditorView {

    private final EditorPresenter editorPresenter;
    private final EditorComponent editorComponent;
    private final DecimalFormat dec = new DecimalFormat("#.##");

    public EditorViewImpl(EditorPresenter editorPresenter) throws HeadlessException {
        this.editorPresenter = editorPresenter;
        this.editorPresenter.setEditorView(this);
        this.setSize(900, 700);
        this.setTitle("graphics editor");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        JPanel shapePanel = new JPanel();
        JPanel colorPanel = new JPanel();

        Box shapeBox = Box.createHorizontalBox();
        Box colorBox = Box.createHorizontalBox();

        JButton brushBut = makeMeButtons("Кисть", 1);
        JButton lineBut = makeMeButtons("Линия", 2);
        JButton ellipseBut = makeMeButtons("Эллипс", 3);
        JButton rectBut = makeMeButtons("Прямоугольник", 4);


        JButton strokeBut = makeMeColorButton("Цвет контура", true);
        JButton fillBut = makeMeColorButton("Цвет заливки", false);

        shapeBox.add(brushBut);
        shapeBox.add(lineBut);
        shapeBox.add(ellipseBut);
        shapeBox.add(rectBut);
        //shapeBox.add(eraserBut);
        colorBox.add(strokeBut);
        colorBox.add(fillBut);

        JLabel transLabel = new JLabel("Прозрачность: 1");

        JSlider transSlider = new JSlider(1, 99, 99);


        transSlider.addChangeListener(e -> {
            if (e.getSource() == transSlider) {
                transLabel.setText("Прозрачность: " + dec.format(transSlider.getValue() * .01));
                editorPresenter.setTransparentValue(transSlider.getValue() * .01f);
            }
        });

        colorBox.add(transLabel);
        colorBox.add(transSlider);

        shapePanel.add(shapeBox);
        colorPanel.add(colorBox);
        this.add(shapePanel, BorderLayout.NORTH);
        this.add(colorPanel, BorderLayout.SOUTH);
        this.editorComponent = new EditorComponent(editorPresenter);
        this.add(editorComponent, BorderLayout.CENTER);

        editorPresenter.startListening();
    }

    private JButton makeMeButtons(String name, int actionNum) {
        JButton theBut = new JButton();
        theBut.setText(name);
        theBut.addActionListener(e -> editorPresenter.setCurrentAction(actionNum));
        return theBut;
    }

    private JButton makeMeColorButton(String name, boolean stroke) {
        JButton theButt = new JButton();
        theButt.setText(name);

        if (stroke) {
            theButt.addActionListener(e -> {
                Color picked = JColorChooser.showDialog(null, "Pick a Stroke", Color.BLACK);
                editorPresenter.setStrokeColor(picked);
            });
        } else {
            theButt.addActionListener(e -> {
                Color picked = JColorChooser.showDialog(null, "Pick a Fill", Color.BLACK);
                editorPresenter.setFillColor(picked);
            });
        }
        return theButt;
    }

    @Override
    public void repaintShapes() {
        this.editorComponent.repaint();
    }

    @Override
    public void showView() {
        this.setVisible(true);
    }
}
