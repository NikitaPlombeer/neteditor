package org.baidina.utils;

import org.baidina.MyShape;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

public class ShapeFactory {

    public static Shape create(MyShape myShape) {
        int[] c = myShape.getCoords();
        switch (myShape.getType()) {
            case LINE:
                return createLine(c[0], c[1], c[2], c[3]);
            case ELLIPSE:
                return createEllipse(c[0], c[1], c[2], c[3]);
            case RECTANGLE:
                return createRectangle(c[0], c[1], c[2], c[3]);
            case BRUSH:
                return createBrush(c[0], c[1], c[2], c[3]);
        }
        return null;
    }

    public static Rectangle2D.Float createRectangle(int x1, int y1, int x2, int y2) {
        int x = Math.min(x1, x2);
        int y = Math.min(y1, y2);

        int width = Math.abs(x1 - x2);
        int height = Math.abs(y1 - y2);
        return new Rectangle2D.Float(x, y, width, height);
    }


    public static Ellipse2D.Float createEllipse(int x1, int y1, int x2, int y2) {
        int x = Math.min(x1, x2);
        int y = Math.min(y1, y2);
        int width = Math.abs(x1 - x2);
        int height = Math.abs(y1 - y2);
        return new Ellipse2D.Float(x, y, width, height);
    }

    public static Ellipse2D.Float createBrush( // кисть
                                               int x1, int y1, int brushStrokeWidth, int brushStrokeHeight) {
        return new Ellipse2D.Float(
                x1, y1, brushStrokeWidth, brushStrokeHeight);
    }

    public static Line2D.Float createLine(int x1, int y1, int x2, int y2) {
        return new Line2D.Float(x1, y1, x2, y2);
    }
}
