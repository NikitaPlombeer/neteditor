package org.baidina.utils;


import org.baidina.MyShape;

import java.awt.*;

public class ShapeDrawer {

    public static void draw(Graphics2D g, MyShape myShape) {
        Shape shape = ShapeFactory.create(myShape);
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, myShape.getAlpha()));

        g.setPaint(new Color(myShape.getStrokeColor()));
        g.draw(shape);

        g.setPaint(new Color(myShape.getFillColor()));
        g.fill(shape);
    }
}
