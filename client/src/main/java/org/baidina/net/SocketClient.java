package org.baidina.net;

import org.baidina.MyShape;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class SocketClient {
    private static SocketClient instance;
    private static final String host = "localhost";
    private static final int port = 2550;
    private final ExecutorService serverProcessingPool = Executors.newFixedThreadPool(10);

    private SocketClient() {
    }

    public static SocketClient getInstance() {
        SocketClient localInstance = instance;
        if (localInstance == null) {
            synchronized (SocketClient.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new SocketClient();
                }
            }
        }
        return localInstance;
    }

    public synchronized void postNewShapeAsync(Object o){
        serverProcessingPool.submit(() -> {
            postNewShapeSync(o);
        });
    }

    private synchronized void postNewShapeSync(Object o){
        // Opening Connection based on the port number 80(HTTP) and 443(HTTPS)
        Socket clientSocket = null;
        ObjectOutputStream request = null;
        try {

            clientSocket = new Socket(host, port);

            System.out.println("======================================");
            System.out.println("Connected");
            System.out.println("======================================");

            request = new ObjectOutputStream(clientSocket.getOutputStream());
            request.writeObject(o);
            request.flush();
            System.out.println("postNewShapeSync Request Sent!");
            System.out.println("======================================");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (request != null) request.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (clientSocket != null) clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized Future<List<MyShape>> getPictureAsync() {
        Callable<List<MyShape>> task = this::getPictureSync;
        return serverProcessingPool.submit(task);

    }

    public synchronized List<MyShape> getPictureSync() {
        // Opening Connection based on the port number 80(HTTP) and 443(HTTPS)
        Socket clientSocket = null;
        ObjectOutputStream request = null;
        ObjectInputStream response = null;
        try {
            clientSocket = new Socket(host, port);

            System.out.println("======================================");
            System.out.println("Connected");
            System.out.println("======================================");

            // Declare a writer to this url
            request = new ObjectOutputStream(clientSocket.getOutputStream());

            request.writeObject("GET PICTURE");
            request.flush();
            System.out.println("getPicture Request Sent!");
            System.out.println("======================================");
            System.out.println("======================================");
            System.out.println("Response Received!!");
            System.out.println("======================================");
            response = new ObjectInputStream(clientSocket.getInputStream());
            return (List<MyShape>) response.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if(response != null) response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(request != null) request.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(clientSocket != null) clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
