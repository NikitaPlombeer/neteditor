package org.baidina;

import org.baidina.mvp.EditorPresenter;
import org.baidina.mvp.EditorPresenterImpl;
import org.baidina.mvp.EditorView;
import org.baidina.mvp.EditorViewImpl;
import org.baidina.net.SocketClient;

public class ClientMain {
    public static void main(String[] args) {
        EditorPresenter editorPresenter = new EditorPresenterImpl(SocketClient.getInstance());
        EditorView editorView = new EditorViewImpl(editorPresenter);
        editorView.showView();
    }
}
